<?php 
require('header.php'); 

require('core/User.php');
$user = new User();

?>

<br>
<br>
<br>
<br>


<!-- Contact Section-->
        <section class="page-section" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Login</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
                    	<?php
								if(isset($_POST['submit'])){
									$result = $user->login($_POST['email'],$_POST['password']);
									/* echo "<pre>";
									print_r($result);die; */
									if(count($result) == 1 ){
										session_start();
										$_SESSION['user_id']= $result[0]['id'];//die;
										$_SESSION['user_name']= $result[0]['name'];
										$_SESSION['user_email']= $result[0]['email'];
										
										echo "<p class='alert alert-danger'>Login Successful</p>";
										header("location: index.php");
									}else{
                                    echo "<p class='alert alert-danger'>Invalid Credentials</p>";
                                }

									
								}

                    	?>
                        <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19.-->
                        <form id="contactForm" name="sentMessage" novalidate="novalidate" action="" method="POST">
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Email</label><input class="form-control" id="email" type="text" placeholder="Name" required="required" data-validation-required-message="Please enter your name." name="email" />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="form-group floating-label-form-group controls mb-0 pb-2">
                                    <label>Password</label><input class="form-control" id="phone" type="password" placeholder="Password" required="required" data-validation-required-message="Please enter your phone number." name="password" />
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                           
                            <br />
                            <div id="success"></div>
                            <div class="form-group"><button class="btn btn-primary " id="sendMessageButton" type="submit" name="submit">Login</button></div>
                        </form>
                    </div>
                </div>
            </div>
        </section>

<?php

include 'footer.php';


?>