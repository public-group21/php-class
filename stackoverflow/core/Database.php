<?php
class Database{
	
	public $conn;
	
	public function __construct(){
		$servername = "localhost";
		$username = "root";
		$password = "";
		
		
		$connect = new PDO("mysql:host=$servername;dbname=stackoverflow", $username, $password);
	
  		$this->conn = $connect;
	}
	
	
	public function dataWrite($sql){
		
		$statement = $this->conn->prepare($sql);
		$statement->execute();
		return $this->conn->lastInsertId();

	}
	
	public function dataFetch($sql){
		
		$statement =  $this->conn->prepare($sql);

		$statement->execute();

		return $statement->fetchAll(PDO::FETCH_ASSOC);
		
	}
	
	
	
}









 ?>