<?php 
require('header.php'); 

require('core/Questions.php');
$question = new Questions();

?>

	<br>
	<br>
	<br>
	<br>
	<section class="page-section" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Write Your Question</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
						<?php if(isset($_POST['submit']) && $_SESSION['user_id']){
							$result = $question->addQuestion($_POST['title'],$_POST['question_detail'],$_SESSION['user_id']);

							if($result > 0){
								echo "<p class='alert alert-success'>Question Post Successful</p>";
							}else{
								echo "<p class='alert alert-danger'>Question Post Faield</p>";
							}
						}
						?>
					

					<form id="contactForm" name="sentMessage" novalidate="novalidate" action="" method="POST">
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Title</label><input class="form-control" id="title" type="text" placeholder="Enter your Question title" required="required" data-validation-required-message="Please enter your name." name="title" />
								<p class="help-block text-danger"></p>
							</div>
						</div>
						
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								
									<textarea id="question" name="question_detail" class="form-control"></textarea>
								
							</div>
						</div>
						
						<br />
						<div id="success"></div>
						<div class="form-group">
							<button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit" name="submit">Post Question</button>
						</div>

						
					</form>
		
		        </div>
            </div>
        </div>
   </section>
   <br>
   <br>
   <br>


		<script>
			
			tinymce.init({
			  selector: '#question'
			});

        </script>
        
<?php

include 'footer.php';


?>