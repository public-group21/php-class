<?php 
require('header.php'); 
require('core\Questions.php');

$questions = new Questions();
 

?>
		<br>
	<br>
	<br>
	<br>
	<section class="page-section" id="contact">
            <div class="container">
                <!-- Contact Section Heading-->
                <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Edit Question Question</h2>
                <!-- Icon Divider-->
                <div class="divider-custom">
                    <div class="divider-custom-line"></div>
                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                    <div class="divider-custom-line"></div>
                </div>
                <!-- Contact Section Form-->
                <div class="row">
                    <div class="col-lg-8 mx-auto">
					<?php 
						$qstn_id = $_GET['question_id'];
						$single_question = $questions->getOneQuestion($qstn_id); 
						
					?>
					
					
					
					<?php if(isset($_SESSION['user_id']) && isset( $_POST['submit']) && $_SESSION['user_id']==$single_question[0]['user_id']){
							$result = $questions->updateQuestion($single_question[0]['id'],$_POST['title'],$_POST['question_detail']);
							
							
							if($result ){
								echo "<p class='alert alert-success'>Question Successfully Updated</p>";
							}else{
								echo "<p class='alert alert-danger'>Question Update Faield</p>";
							}
						}
					?>
					
					<form id="contactForm" name="sentMessage" novalidate="novalidate" action="" method="POST">
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								<label>Title</label><input class="form-control" id="title" type="text" placeholder="Enter your Question title" value="<?php echo $single_question[0]['question_title']; ?>" required="required" data-validation-required-message="Please enter your name." name="title" />
								<p class="help-block text-danger"></p>
							</div>
						</div>
						
						<div class="control-group">
							<div class="form-group floating-label-form-group controls mb-0 pb-2">
								
									<textarea id="question" name="question_detail" class="form-control"><?php echo $single_question[0]['question_detail']; ?></textarea>
								
							</div>
						</div>
						
						<br />
						<div id="success"></div>
						<div class="form-group">
							<button class="btn btn-primary btn-xl" id="sendMessageButton" type="submit" name="submit">Update Question</button>
						</div>

						
					</form>
		
		        </div>
            </div>
        </div>
   </section>
   <br>
   <br>
   <br>










		<script>
			
			tinymce.init({
			  selector: '#question'
			});

        </script>
        </script>

<?php require('footer.php'); ?> 