@extends('frontend.layouts.layouts')
@push('css')
    
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/dataTables.bootstrap5.min.css">
    <style>
            .tableSm > thead > tr th{
                font-weight:bold;
            }
            #contactData tr th{
                text-align:center;
            }
    </style>
@endpush
@section('content')
<div class="small-box max-auto text-center">
    <table id="contactData" class="table table-striped tableSm " style="width:100%">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone No</th>
                <th>Entry Date</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if(!empty($all_contact))
                @foreach($all_contact as $contact)
                    <tr>
                        <td>{{ $contact->name }}</td>
                        <td>{{ $contact->email }}</td>
                        <td>{{ $contact->contact }}</td>
                        <td>{{ $contact->created_at }}</td>
                        <td><a href="{{ route('editContact',$contact->id) }}" class="btn btn-primary">Edit</a> <a href="{{ route('deleteContact', $contact->id) }}" class="btn btn-danger" onclick="return confirm('Are You Sure to Delete')"">Delete</a></td>
                       
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="5">No Contact found</td>
                </tr>  
            @endif
           
         </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Phone No</th>
                <th>Entry Date</th>
                <th>Action</th>
            </tr>
            </tr>
        </tfoot>
    </table>
</div>
@endsection   
@push('jsadd')
    <script src="https://code.jquery.com/jquery-3.5.1.js" ></script>
    <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js" ></script>
    <script src="https://cdn.datatables.net/1.12.1/js/dataTables.bootstrap5.min.js"></script>

    <script>
        $(document).ready(function () {
             $('#contactData').DataTable();
        });
    </script>

@endpush