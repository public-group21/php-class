@extends('frontend.layouts.layouts')
@section('content')
    <div class="page-header">
      <h3 class="page-title"> Here You Have to @if(isset($sontactDetails) && !empty($sontactDetails->id)) Update  @else Store @endif Contact</h3>
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="#">Contact</a></li>
          <li class="breadcrumb-item active" aria-current="page">Save Contacts</li>
        </ol>
      </nav>
    </div>
    <div class="row">
      <div class="col-12 grid-margin stretch-card">
        <div class="card">
          <div class="card-body">
            <h4 class="card-title">@if(isset($sontactDetails) && !empty($sontactDetails->id)) Update Contact @else Store Contact @endif</h4>
            <p class="card-description">Put Contact Detail </p>
            <form class="forms-sample" action="@if(isset($sontactDetails) && !empty($sontactDetails->id)){{ route('updateContact',$sontactDetails->id) }}@else{{ route('phonebookData') }}@endif" method="post">
              <div class="small-box text-center mx-auto bg-success rounded mb-2">
                <span class="text-white">@if (session('success')){{ session('success') }} @endif </span> 
              </div>
              
              {{ csrf_field() }}
              <div class="form-group">
                <label for="unameid">Name</label>
                <input type="text" class="form-control" name="uname" id="unameid" placeholder="Name" value="@if(isset($sontactDetails) && !empty($sontactDetails->name)){{ $sontactDetails->name }}@else{{ old('uname') }}@endif" >
                <span class="text-danger">@if ($errors->has('uname')){{ $errors->first('uname') }}</span> @endif </span>
              </div>
              <div class="form-group">
                <label for="emailid">Email address</label>
                <input type="email" name="uemail" class="form-control" id="emailid" placeholder="Email" value="@if(isset($sontactDetails) && !empty($sontactDetails->email)){{ $sontactDetails->email }}@else{{ old('uemail') }}@endif" >
                <span class="text-danger">@if ($errors->has('uemail')){{ $errors->first('uemail') }}</span> @endif </span>
              </div>
              <div class="form-group">
                <label for="unumberid">Phone No</label>
                <input type="text" name="unumber"class="form-control" id="unumberid" placeholder="Phone Number" value="@if(isset($sontactDetails) && !empty($sontactDetails->contact)){{ $sontactDetails->contact }}@else{{ old('unumber') }}@endif">
                <span class="text-danger">@if ($errors->has('unumber')){{ $errors->first('unumber') }}</span> @endif </span>
              </div>
              
              <!-- <div class="form-group">
                <label>File upload</label>
                <input type="file" name="img[]" class="file-upload-default">
                <div class="input-group col-xs-12">
                  <input type="text" class="form-control file-upload-info" disabled placeholder="Upload Image">
                  <span class="input-group-append">
                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                  </span>
                </div>
              </div> -->
              
              <button type="submit" class="btn btn-primary mr-2">Submit</button>
              <button class="btn btn-light">Cancel</button>
            </form>
          </div>
        </div>
      </div>
    </div>
@endsection