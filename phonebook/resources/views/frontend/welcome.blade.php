
@extends('frontend.layouts.layouts')
@section('content')

  <!-- Quick Action Toolbar Starts-->
  <div class="row quick-action-toolbar">
    <div class="col-md-12 grid-margin">
      <div class="card">
        <div class="card-header d-block d-md-flex">
          <h5 class="mb-0">Quick Actions</h5>
          <p class="ml-auto mb-0">How are your active users trending overtime?<i class="icon-bulb"></i></p>
        </div>
        <div class="d-md-flex row m-0 quick-action-btns" role="group" aria-label="Quick action buttons">
          <div class="col-sm-6 col-md-3 p-3 text-center btn-wrapper">
            <button type="button" class="btn px-0"> <i class="icon-user mr-2"></i> Add Contact</button>
          </div>
          
          <div class="col-sm-6 col-md-3 p-3 text-center btn-wrapper">
            <button type="button" class="btn px-0"><i class="icon-folder mr-2"></i> All Contact</button>
          </div>
          
        </div>
      </div>
    </div>
  </div>
  <!-- Quick Action Toolbar Ends-->


  <div class="row">
    <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="d-sm-flex align-items-center mb-4">
            <h4 class="card-title mb-sm-0">Contact List</h4>
            <a href="#" class="text-dark ml-auto mb-3 mb-sm-0"> View all Contact</a>
          </div>
          <div class="table-responsive border rounded p-1">
            <table class="table">
              <thead>
                <tr>
                  <th class="font-weight-bold">Name</th>
                  <th class="font-weight-bold">Mobile</th>
                  <th class="font-weight-bold">Email</th>
                  <th class="font-weight-bold">Entry Date</th>
                  
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <img class="img-sm rounded-circle" src="{{ asset('fontend/images/faces/face1.jpg') }}" alt="profile image"> Katie Holmes
                  </td>
                  <td>$3621</td>
                  <td><img src="{{ asset('fontend/images/dashboard/alipay.png') }}" alt="alipay" class="gateway-icon mr-2"> alipay</td>
                  <td>04 Jun 2019</td>
                
                  
                </tr>
                <tr>
                  <td>
                    <img class="img-sm rounded-circle" src="{{ asset('fontend/images/faces/face2.jpg') }}" alt="profile image"> Minnie Copeland
                  </td>
                  <td>$6245</td>
                  <td><img src="{{ asset('fontend/dashboard/paypal.png') }}" alt="alipay" class="gateway-icon mr-2"> Paypal</td>
                  <td>25 Sep 2019</td>
                  
                </tr>
                <tr>
                  <td>
                    <img class="img-sm rounded-circle" src="{{ asset('fontend/images/faces/face3.jpg') }}" alt="profile image"> Rodney Sims
                  </td>
                  <td>$9265</td>
                  <td><img src="{{ asset('fontend/images/dashboard/alipay.png') }}" alt="alipay" class="gateway-icon mr-2"> alipay</td>
                  <td>12 dec 2019</td>
                  
                </tr>
                <tr>
              
              </tbody>
            </table>
          </div>
          <div class="mt-4 ">
            <a href="#" style="float:right; text-decoration:none"">View All Contact</a>
          </div>
        </div>
      </div>
    </div>
  </div>
          
          <!-- content-wrapper ends -->
@endsection          
        