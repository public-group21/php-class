<!DOCTYPE html>
<html>
    <head>
        
        <link rel="stylesheet" href="{{ asset('fontend/css/css_login.css') }}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
		<!-- Latest compiled and minified CSS -->
        @stack('css')
		
            <meta charset="UTF-8">
            <meta name="description" content="Free Web tutorials">
            <meta name="keywords" content="HTML, CSS, JavaScript">
            <meta name="author" content="John Doe">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Phone Book</title>
        
    </head>

        <body>


            @yield('content')


            <script>

                //====== Thiis is for Small Device Menue to Large device Good Responce ==================//    
                const navToggel = document.querySelector(".navbar-toggler");
                const nav = document.querySelector("#mt");
                        
                navToggel.addEventListener("click", () => {
                    nav.classList.toggle("dis-pro");
                    
                });  

                window.addEventListener('resize', (event) => {
                    const windowWidth = window.innerWidth;
                        if(windowWidth > 715){

                            nav.classList.remove("dis-pro");
                        }
                    
                });
            
            </script>
            @stack('jss')

    </body>

</html>