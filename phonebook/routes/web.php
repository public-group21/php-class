<?php
use App\Http\Controllers\PhonebookController;
use App\Http\Controllers\UserController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class,'loginView'])->name('login');

Route::post('/login',[UserController::class,'login'])->name('userLogin');

Route::get('user/signup', function () {
    return view('frontend.signup');
})->name('register');

Route::post('user/storeUser',[UserController::class,'store_current_user'])->name('storeUser');
Route::group(['middleware'=>'authCheck', 'prefix'=>'contact'],function(){

    Route::get('contact_entry', function () {
        return view('frontend.contact_entry');
    })->name('contact');
    Route::get('all_entry',[PhonebookController::class,'showallContentact'])->name('all.contact');
    Route::post('phonebookData',[PhonebookController::class,'phonebook_data'])->name('phonebookData');
    Route::get('editContact/{id}',[PhonebookController::class,'edit_contact_data'])->name('editContact');
    Route::post('updateContact/{id}',[PhonebookController::class,'update_contact_data'])->name('updateContact');
    Route::get('deleteContact/{id}',[PhonebookController::class,'delete_contact'])->name('deleteContact');
    Route::get('cuLogout',[UserController::class,'userLogout'])->name('uLogout');
   

});

  
   

Route::get('google/redirect',[UserController::class,'getSocialInfo'])->name('GloginRd');
Route::get('google/callback',[UserController::class,'getSocialInfoBack'])->name('GloginBk');

?>