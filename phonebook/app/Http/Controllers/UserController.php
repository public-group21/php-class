<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;


class UserController extends Controller
{
    public function store_current_user(){
        $this->validate(request(),[
            'first_name'=>'required|min:2',
            'last_name'=>'required|min:3',
            'your_email'=>'required|email|unique:users,email',
            'phone_number'=>'min:10|numeric',
            'pass'=>'required|min:8|confirmed',
            'pass_confirmation'=>'required|min:8|required_with:pass|same:pass',
         ],
         [
            'first_name.required'=>'First name field can\'t empty!',
            'last_name.required'=>'Last name field can\'t empty!',
            'your_email.required'=>'Email field can\'t empty!',
            //'phone_number.required'=>'Number field can\'t empty!',
            'phone_number.min'=>'Number must be at least 10!',
            'phone_number.numeric'=>'Number field can take only number!',
            'pass.required'=>'Password name can\'t empty!',
            'pass.min'=>'Password must be at least 8 character!',
            'pass_confirmation.required'=>'Confirm Password name can\'t empty!',
            'pass_confirmation.min'=>'Confirm Password must be at least 8 character!',
            'pass_confirmation.required_with'=>'Please Confirmed Password!',
            'pass_confirmation.same'=>'Password Not Matched!',
            //'password.alpha_num'=>'Password must be alphabet and numaric only',
            
            
         ]);


         
         User::create([
            'first_name'=> request('first_name'),
            'last_name'=> request('last_name'),
            'email'=> request('your_email'),
            'phone'=> request('phone_number'),
            'password'=> bcrypt(request('pass'))
         ]);
   
         return redirect()->back()->with('success','true');
    }

    

    public function loginView(){
        if(Auth::check()){
            return view('frontend.contact_entry');  
        }
        return view('frontend.login');
    }

    public function login(){
        $this->validate(request(),[
            'email'=>'required|min:2',
            'password'=>'required|min:5'
        ]);
        
        if(
            Auth::attempt([
            'email'=>request('email'),
            'password'=>request('password')

            ])
        ){
            return redirect()->route('contact');
        }else{
            return redirect()->back();
        }

    }

    public function userLogout(){
        Auth::logout();
        

        return redirect()->route('login');

    }

  
    public function getSocialInfo(){
        return Socialite::driver('google')->redirect();
    }
    
    public function getSocialInfoBack(){
        $user = Socialite::driver('google')->user();
        $name = explode(' ',$user->getName());
        $first_name = $name[0];
        $last_name = end($name);
        $email = $user->getEmail(); 

        $getUser = User::where('email',$email)->first();
        if($getUser){
            Auth::login($getUser);
            return redirect()->route('all.contact');
        }else{
           $currentUser = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => '1234567890',
                'email' => $email, 
                'password' => bcrypt('Qwe@123453')
            ]);

            Auth::login($currentUser);

            return redirect()->route('all.contact');
        } 
      
    }
}
