<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PhoneBook;
use Illuminate\Support\Facades\Auth;

class PhonebookController extends Controller
{

   public function showallContentact(){

      $all_contact = PhoneBook::all();   
      return view('frontend.all_contact',compact('all_contact'));
   }

   public function phonebook_data(){

      $this->validate(request(),[
         'uname'=>'required|min:3',
         'uemail'=>'required|email',
         'unumber'=>'required|min:10|numeric',
      ],
      [
         'uname.required'=>'Contact name field can\'t empty!',
         'uemail.required'=>'Email field can\'t empty!',
         'unumber.required'=>'Number field can\'t empty!',
         'unumber.min'=>'Number must be at least 10!',
         'unumber.numeric'=>'Number field can take only number!'
         
      ]);

      PhoneBook::create([
		 'user_id'=> Auth::user()->id,
         'name'=> request('uname'),
         'email'=> request('uemail'),
         'contact'=> request('unumber')
      ]);

      return redirect()->back()->with('success','Contact added Successfully!');
       // dd(request()->all());
   }

   public function edit_contact_data($id){
     // $sontactDetails =  PhoneBook::where('id',$id)->first();
     $sontactDetails =  PhoneBook::find($id);   // both are same
     return view('frontend.contact_entry',compact('sontactDetails'));
   }

   public function update_contact_data($id){

      $this->validate(request(),[
         'uname'=>'required|min:3',
         'uemail'=>'required|email',
         'unumber'=>'required|min:10|numeric',
      ],
      [
         'uname.required'=>'Contact name field can\'t empty!',
         'uemail.required'=>'Email field can\'t empty!',
         'unumber.required'=>'Number field can\'t empty!',
         'unumber.min'=>'Number must be at least 10!',
         'unumber.numeric'=>'Number field can take only number!'
         
      ]);


      $sontactDetails =  PhoneBook::find($id); 

      $sontactDetails->update([
         'name'=> request('uname'),
         'email'=> request('uemail'),
         'contact'=> request('unumber')
      ]);

      return redirect()->route('all.contact');
   }

   public function delete_contact($id){
       PhoneBook::findOrFail($id)->delete(); 
       
       return redirect()->route('all.contact');
   }

}
