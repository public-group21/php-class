<?php
use App\Http\Controllers\PhonebookController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AdminController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [UserController::class,'loginView'])->name('login');

Route::post('/login',[UserController::class,'login'])->name('userLogin');

Route::get('user/signup', function () {
    return view('frontend.signup');
})->name('register');

Route::post('user/storeUser',[UserController::class,'store_current_user'])->name('storeUser');

Route::group(['middleware'=>'authCheck', 'prefix'=>'user'],function(){

    
    Route::get('dash',[UserController::class,'user_dash'])->name('user.dash');
    Route::get('get-ticket',[UserController::class,'get_tickets'])->name('get.tickets');
    Route::get('get-new-ticket',[UserController::class,'get_new_tickets'])->name('get.new.tickets');
    Route::get('user-profile',[UserController::class,'get_user_profile'])->name('get.user.profile');
    Route::post('user-profile-update',[UserController::class,'user_profile_update'])->name('user.profile.update');
    Route::get('cuLogout',[UserController::class,'userLogout'])->name('uLogout');

   

});

Route::get('google/redirect',[UserController::class,'getSocialInfo'])->name('GloginRd');
Route::get('google/callback',[UserController::class,'getSocialInfoBack'])->name('GloginBk');

Route::get('admin/login', [AdminController::class,'AdminloginView'])->name('admin.login');
Route::post('admin/login',[AdminController::class,'Adminlogin'])->name('adminLogin');
Route::get('admin/dashboard', function () {
    return view('admin.index');
})->name('admin_dashboard');
Route::get('caLogout',[AdminController::class,'adminLogout'])->name('aLogout');




?>