<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Image;
use App\Models\Ticket;
use App\Models\Helper;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function store_current_user(){
        $this->validate(request(),[
            'first_name'=>'required|min:2',
            'last_name'=>'required|min:3',
            'your_email'=>'required|email|unique:users,email',
            'phone_number'=>'min:10|numeric',
            'pass'=>'required|min:8|confirmed',
            'pass_confirmation'=>'required|min:8|required_with:pass|same:pass',
         ],
         [
            'first_name.required'=>'First name field can\'t empty!',
            'last_name.required'=>'Last name field can\'t empty!',
            'your_email.required'=>'Email field can\'t empty!',
            //'phone_number.required'=>'Number field can\'t empty!',
            'phone_number.min'=>'Number must be at least 10!',
            'phone_number.numeric'=>'Number field can take only number!',
            'pass.required'=>'Password name can\'t empty!',
            'pass.min'=>'Password must be at least 8 character!',
            'pass_confirmation.required'=>'Confirm Password name can\'t empty!',
            'pass_confirmation.min'=>'Confirm Password must be at least 8 character!',
            'pass_confirmation.required_with'=>'Please Confirmed Password!',
            'pass_confirmation.same'=>'Password Not Matched!',
            //'password.alpha_num'=>'Password must be alphabet and numaric only',
            
            
         ]);


         
         User::create([
            'first_name'=> request('first_name'),
            'last_name'=> request('last_name'),
            'email'=> request('your_email'),
            'phone'=> request('phone_number'),
            'password'=> bcrypt(request('pass'))
         ]);
   
         return redirect()->back()->with('success','true');
    }

    

    public function loginView(){
        if(Auth::check()){
            return view('frontend.dashboard');  
        }
        return view('frontend.login');
    }

    public function login(){

        //dd(request());
        $this->validate(request(),[
            'email'=>'required|min:2',
            'password'=>'required|min:5'
        ]);
        
        if(
            Auth::attempt([
            'email'=>request('email'),
            'password'=>request('password')

            ])
        ){
            return redirect()->route('user.dash');
        }else{
            return redirect()->back();
        }

    }

    public function userLogout(){
        Auth::logout();
        

        return redirect()->route('login');

    }

    public function user_dash(){

        return view('frontend.dashboard');  
    }

    public function get_new_tickets(){
        
        $randNumber = rand(1000,9999);
        $this_year = date('y');
        $this_month = date('m');
        $this_day = date('d');
        $rand_letter="";
        for($i=0;$i<=1;$i++){
            $int = rand(0,25);
            $A_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $rand_letter .= $A_Z[$int];
        }
        
        Ticket::create([
            'user_id'=> Auth::user()->id,
            'ticket_no'=> 'WIZ'.' '.$randNumber.$this_day.$this_month.$this_year .' '.$rand_letter,
            
         ]);
   
         return redirect()->back()->with('success','true');
    }

    public function get_user_profile(){
        return view('frontend.profile');
    }

    public function user_profile_update(Request $request){

        $this->validate($request,[
            'profile_picture' => 'required|image|mimes:jpg,jpeg,png|max:2048'
        ]);
         $ogr_image = $request->file('profile_picture');

         $image_id = Helper::do_file_upload($ogr_image ,'users','uploads/images',Auth::user()->id);

        if($image_id > 0){
            DB::table('users')->where('id',Auth::user()->id)->update([
                'profile_picture_id'=> $image_id 
            ]);

        return redirect()->back();

        }
    }
  
    public function getSocialInfo(){
        return Socialite::driver('google')->redirect();
    }
    
    public function getSocialInfoBack(){
        $user = Socialite::driver('google')->user();
        $name = explode(' ',$user->getName());
        $first_name = $name[0];
        $last_name = end($name);
        $email = $user->getEmail(); 

        $getUser = User::where('email',$email)->first();
        if($getUser){
            Auth::login($getUser);
            return redirect()->route('all.contact');
        }else{
           $currentUser = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'phone' => '1234567890',
                'email' => $email, 
                'password' => bcrypt('Qwe@123453')
            ]);

            Auth::login($currentUser);

            return redirect()->route('all.contact');
        } 
      
    }
}
