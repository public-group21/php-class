<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function AdminloginView(){
        if(Auth::guard('admin')->check()){
            return view('admin.index');  
        }
        return view('admin.login');
    }

    public function Adminlogin(){

       // dd(request());
        $this->validate(request(),[
            'email'=>'required|min:2',
            'password'=>'required|min:5'
        ]);
        
        if(
            Auth::guard('admin')->attempt([
            'email'=>request('email'),
            'password'=>request('password')

            ])
        ){
            return redirect()->route('admin_dashboard');
        }else{
            return redirect()->back();
        }

    }

    public function adminLogout(){
        Auth::guard('admin')->logout();
        
        return redirect()->route('admin.login');

    }

}
