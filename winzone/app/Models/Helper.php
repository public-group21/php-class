<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Models\Image;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;

class Helper extends Model
{
    public static function do_file_upload($file,$parent_table_name,$upload_basic_path="uploads/images",$user_id){
               
        try{
           
            $fileNameWithExt = $file->getClientOriginalName();
            $file_extension = $file->extension();
            $this_year = date('Y');
            $this_month = date('m');
            $this_day = date('d');
            $randNumber = rand(10000,99999);
            $rand_letter="";
            for($i=0;$i<=1;$i++){
                $int = rand(0,25);
                $A_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $rand_letter .= $A_Z[$int];
            }
            $file_new_name = "WINZ_".$this_day."_".$this_month."_".$this_year."_".$randNumber.$rand_letter.".".$file_extension;
            $file_size=$file->getSize();
            $mimeType = $file->getMimeType();
            $destination_folder = $upload_basic_path.'/'.$this_year.'/'.$this_month.'/'.$this_day; 
            $destinationPath = public_path('/'.$destination_folder);
            if(!File::exists($destinationPath)){
                   File::makeDirectory($destinationPath, $mode = 0777, true, true);
            }
                
            $file->move($destinationPath, $file_new_name);
    
            $currentFile = Image::create([
                'user_id' =>$user_id,
                'image_name' => $file_new_name,
                'image_type' => $mimeType,
                'image_location'=> $destination_folder,
                'table_name' => $parent_table_name, 
                'image_size' => $file_size, 
                'status' => 1
            ]);

            if($currentFile->id > 0){
                return $currentFile->id;  
            }
             
        }catch(Exception $e){
            echo "Error when uploading file";
        }

     
    }

    public static function get_resource_file($table,$id){
       $file_data = DB::table($table)->where('id',$id)->first();
       if($file_data !=''){
            return $file_data;
       }else{
        return 0;
       }
    } 
}
