
@extends('frontend.layouts.master')
@section('content')
       
    <div class="banner">
        <div class="main-header">
            <header>
                <nav class="navbar">
                    <div class="nav-btn">
                    <a href="#" class="navbar-brand">Win<span style="color:green; font-size:35px; font-weight:bold">Z</span><span style="color:green">one</span></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="nav-ul-container" id="">
                        <ul class="nav-ul" id="mt">
                            <li class=""><a href="" >Home</a></li>
                            <li class=""><a href="" >About</a></li>
                            <li class=""><a href="" >Contact</a></li>
                        </ul>
                    </div>	
                </nav>
            </header>
        </div>
        

        <div class="main-body-100" id="myd">
            <div class="form-alignment">
                @if(session('success'))<input type="hidden" name="success" id="success" value="{{ session('success') }}"> @endif 
                <form class="form-lg-700" autocomplete="off" action="{{ route('storeUser') }}" method="POST">
                    
                    {{ csrf_field() }}
                    <h2 class=""> Signup Here </h2>
                    <div>
                        <input type="text" name="first_name" id="" placeholder="Enter First Name" autocomplete="off" min="3" required>
                        <input type="text" name="last_name" id="" placeholder="Enter last Name" autocomplete="off" required min="3">
                        <input type="email" name="your_email" id="" placeholder="Enter email here" autocomplete="off" required min="7">
                        <input type="number" name="phone_number" id="" placeholder="Enter phone no" autocomplete="off" required min="10">
                        <input type="password" name="pass" id="" placeholder="Enter password" autocomplete="off" required min="6">
                        <input type="password" name="pass_confirmation" id="" placeholder="Confirm password " autocomplete="off" required min="6">
                        <br>
                        <button class="btnn-lg" name="signup">Sign up</button>
                    </div>
                
                    
                    <p class="text-lg text-sm">
                        Already have an account<br>
                    <a href="{{ route('login') }}">Login</a> here 
                    </p>
                    <p class="plain-text-lg">Login with</p>
                    <div class="social-ico-lg">
                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-google" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>

                    </div>
                </form>  
            </div> 
        </div>
    </div>
 @endsection   
 @push('jss') 
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
            <script>
            let success = document.getElementById('success');

                          
            if(success.value === 'true' ){

                swal({
                    title: "Registration!",
                    text: "You Registration Successful!",
                    icon: "success",
                    button: "Ok",
                });

                setTimeout(function() {
                   
                   window.location = " {{ route('login') }} "; 
               
               }, 1500);  
                

            }
       </script>
   
 @endpush 


 
