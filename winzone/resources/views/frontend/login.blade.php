@extends('frontend.layouts.master')
@section('content')
     
    <div class="banner">
        <div class="main-header">
            <header>
                <nav class="navbar">
                    <div class="nav-btn">
                    <a href="#" class="navbar-brand">Win<span style="color:green; font-size:35px; font-weight:bold">Z</span><span style="color:green">one</span></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <i class="fa fa-bars" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="nav-ul-container" id="">
                        <ul class="nav-ul" id="mt">
                            <li class=""><a href="" >Home</a></li>
                            <li class=""><a href="" >About</a></li>
                            <li class=""><a href="" >Contact</a></li>
                        </ul>
                    </div>	
                </nav>
            </header>
        </div>
    

        <div class="main-body" id="myd">

            <div class="content">
                <h1>Webdesign & <br> <span>Devolopment</span>Course</h1><br>
                
                <p class="par">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores repellat <br>
                    soluta nulla veniam ipsa, ad voluptatibus corporis exercitationem accusantium sintfa-border <br>
                    beatae fuga. Libero quibusdam laudantium iste sequi in vitae temporibus!
                </p>
            </div>
        
            <div class="form-alignment">
                <form class="form-sm-300" autocomplete="off" action="{{ route('userLogin') }}" method="post">
                    {{ csrf_field() }}
                    <h2>Login Here</h2>
                    <div>
                        <input type="email" name="email" id="" placeholder="Enter email here" autocomplete="off">
                        <input type="password" name="password" id="" placeholder="Enter password here" autocomplete="off">
                        <br>
                        <button class="btnn" name="login" value="login">Login</button>
                    </div>
                    
                    <p class="text-sm"">
                        Dont't have an account<br>
                    <a href="{{ route('register') }}">Sign up</a> here 
                    </p>
                    <p class="plain-text-sm">Login with</p>
                    <div class="social-ico">
                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-facebook-square" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="{{ route('GloginRd') }}"><i class="fa fa-google" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-linkedin" aria-hidden="true"></i></a>

                    </div>
                </form>


            </div> 
                    
        
            
        </div>
                    
    </div>

@endsection    

@push('jss')
    <script> </script>
@endpush
