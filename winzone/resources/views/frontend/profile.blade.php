﻿
@extends('frontend.layouts.layouts')
@section('content')
            <!-- Vertical Layout -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                PROFILE DETAILS
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <form action="{{ route('user.profile.update') }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <label for="profile_picture">Profile Picture</label>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="file" name="profile_picture" id="profile_picture" class="form-control">
                                    </div>
                                </div>
                                                               
                                <br>
                                <button type="submit" name="submit" class="btn btn-primary m-t-15 waves-effect">UPDATE</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Vertical Layout -->
            
            <!-- #END# Multi Column -->
        </div>
    </section>
@endsection